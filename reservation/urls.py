from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r"allhotels", HotelList),
    url(r"customers", CustomerList),
    url(r"reservations", ReservationList),
]