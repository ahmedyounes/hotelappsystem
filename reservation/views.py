# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
from .models import *

#Views considerent the middle point between model/database and templates/html
# Create your views here.
#by default you are looking in template folder in reservation ( why reservation folder is default folder ? pending question)
def welcomePage(request):
    return render(request, "welcome.html")

def HotelList(request):
    return render(request, "reservation/listofhotels.html", {"hotels" : Hotel.objects.all()})

def CustomerList(request):
    return render(request, "reservation/customers.html", {"customer" : Customer.objects.all()})

def ReservationList(request):
    return render(request, "reservation/reservations.html", {"reservation" : Reservation.objects.all()})

def HotelInCity(request):
    hotels_incity = "<ul>"
    for i in Hotel.objects.all():
        hotels_incity += "<li>" + i.hotel_name + " in " + i.hotel_city + "</li>"
    hotels_incity += "</ul>"
    return HttpResponse(hotels_incity)

# def ReservationList(request):
#     reservations = "<ul>"
#     for i in Reservation.objects.all():
#         reservations += "<li>" + "Customer : " + i.customer.customer_name + " in " + i.hotel.hotel_name + " hotel from : " + str(i.start_time)[:-9] + " to : "+ str(i.end_time)[:-9] +"</li>"
#     reservations += "</ul>"
#     return HttpResponse(reservations)
