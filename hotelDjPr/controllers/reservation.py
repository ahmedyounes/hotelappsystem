from hotel import Hotel
from customer import Customer
from notification import Notification

class Reservation():
    reservations =[]
    # @classmethod
    def reserve_room(self,hotel_name, customer_name):
        x = Hotel.hotels.values()
        for i in x:
            if hotel_name == i[1]:
                Reservation.reservations.append([hotel_name,customer_name])
                i.empty_rooms -=1
                for i in Customer.customers:
                    if customer_name in i:
                        notification = Notification()
                        notification.send_text_message(customer_name,"message")
    # @staticmethod
    def list_resevrations_for_hotel(self,hotel_name):
        for i in Reservation.reservations:
            if hotel_name in i:
                return "Reservations : ",i
            else:
                return "No reservations available!"