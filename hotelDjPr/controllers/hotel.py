from customer import Customer
# from reservation import Reservation


class Hotel():
    """
    this is hotel class file
    """
    hotels = {}
    
    def __init__(self,number,hotel_name,city,total_number,empty_rooms):
        self.number = number
        self.hotel_name = hotel_name
        self.city = city
        self.total_number = total_number
        self.empty_rooms = empty_rooms

        if self.hotel_name in Hotel.hotels:
            return

        Hotel.hotels[self.hotel_name] = self


    def list_hotels_in_city(self,city):
        for i in Hotel.hotels.values():
            if city in i.city:
                return "In ",city,": ",i.hotel_name,"hotel, available rooms :",i.empty_rooms


    def __repr__(self):
        """print output when printing them inside a list"""
        return repr([self.number,self.hotel_name,self.city,
                     self.total_number,self.empty_rooms])
    
        
    def __getitem__(self, hotel_name):
          return self.hotel_name

